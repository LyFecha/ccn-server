import CardState from "../model/cardState";
import { Arrow, ArrowTable } from "./arrows";
import Status from "../model/status";

const activate = (field: Array<CardState | null>) => {
  const checked_cards: Array<CardState> = [];
  const modified_field: Array<CardState | null> = Array(9)
    .fill(null)
    .concat(
      field.slice(0, 6),
      [null, null],
      field.slice(6, 12),
      [null, null],
      field.slice(12, 18),
      Array(9).fill(null)
    );
  modified_field.forEach((card, index) => {
    if (card && !checked_cards.includes(card)) {
      const group: Array<CardState> = [card];

      getConnectedGroup(card, index, group, checked_cards, modified_field);

      console.log(group);

      if (
        group.length > 2 ||
        (group.length === 2 &&
          ((group[0].status & Status.ACTIVATED) > 0 ||
            (group[1].status & Status.ACTIVATED) > 0))
      ) {
        group.forEach((card) => {
          card.status = card.status | Status.ACTIVATED;
        });
      }
    }
  });
};

export default activate;

export const getConnectedGroup = (
  card: CardState,
  index: number,
  group: Array<CardState>,
  checked_cards: Array<CardState>,
  modified_field: Array<CardState | null>
) => {
  checked_cards.push(card);
  Object.keys(Arrow).forEach((key, ind) => {
    if ((Arrow[key][1] & card.arrows) > 0) {
      const pointed_index = index + Arrow[key][0];
      const pointed_card = modified_field[pointed_index];
      if (
        pointed_card &&
        !group.includes(pointed_card) &&
        (ArrowTable[(ind + 4) % 8][1] & pointed_card.arrows) > 0 &&
        pointed_card.isArrowBlue === card.isArrowBlue &&
        !(
          (pointed_card.status & (Status.ACTIVATED | Status.NON_CONDUCTIVE)) ==
          (Status.ACTIVATED | Status.NON_CONDUCTIVE)
        ) &&
        !((pointed_card.status & Status.CAGED) > 0)
      ) {
        group.push(pointed_card);
        getConnectedGroup(card, index, group, checked_cards, modified_field);
      }
    }
  });
};
