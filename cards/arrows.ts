export const Arrow: { [index: string]: number[] } = {
  ARROW_UL: [-9, 1 << 0],
  ARROW_UC: [-8, 1 << 1],
  ARROW_UR: [-7, 1 << 2],
  ARROW_MR: [1, 1 << 3],
  ARROW_BR: [9, 1 << 4],
  ARROW_BC: [8, 1 << 5],
  ARROW_BL: [7, 1 << 6],
  ARROW_ML: [-1, 1 << 7],
};

export const ArrowTable: Array<Array<number>> = [
  [-9, 1 << 0],
  [-8, 1 << 1],
  [-7, 1 << 2],
  [1, 1 << 3],
  [9, 1 << 4],
  [8, 1 << 5],
  [7, 1 << 6],
  [-1, 1 << 7],
];

/*
0 1 2 3 4 5 6 7
X X X X X X X X +0
X O O O O O O X +8
X O O O O O O X +16
X O O O O O O X +24
X X X X X X X X +32

9 :

0  1  2
8  9  10
16 17 18
*/
