import Status from "../model/status";
import CardsId from "../model/cardsId";
import Game from "../model/game";
import CardState from "../model/cardState";
import StatusName from "../model/statusName";
import Cards from "../model/cards";
import { ArrowTable } from "./arrows";
import { shuffle } from "../constants";
import { getConnectedGroup } from "./activate";
import Base from "../model/base";

export enum Trigger {
  PLACEMENT = 1 << 0,
  ACTIVATE = 1 << 1,
  RESOLVE = 1 << 2,
  CARD_PLAY = 1 << 3,
  DRAW = 1 << 4,
  DAMAGE = 1 << 5,
  HEAL = 1 << 6,
  SHIELD = 1 << 7,
}

type Data = {
  cardPlayed?: CardState;
  cardPlayedId?: number;
  amount?: number;
  toBlue?: boolean;
  placementId?: number;
};

/*
    if ((trigger & Trigger.PLACEMENT) > 0) {
      
    } else if ((trigger & Trigger.RESOLVE) > 0) {

    }
*/

// prettier-ignore
const CardsEffect: { [index: string]: Function } = {
  // Obtainable
  //1
  ADWOA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card);
      damage(game, card);
    }
  },
  ALEXANDER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  ALF: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      peekAtDeck(game, card)
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card);
    }
  },
  ALPHA_STRIKE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const cardsOnField : Array<{index : number, card : CardState}> = [];
      game.card.field.forEach((cardField, ind) =>
        (cardField && card.delay > 0) ? cardsOnField.push({index: ind, card: cardField}) : null
      )
      const cardKeptInd = cardsOnField[getRandomInt(cardsOnField.length)].index;
      game.card.field.map((cardField, ind) => 
        (ind == cardKeptInd || (cardField && cardField.delay <= 0)) ? cardField : null
      )
      game.logs.push(`Alpha Strike : Toutes les cartes ont été supprimées sauf ${game.card.field[cardKeptInd]!.name}`);
    }
  },
  ANCIENT_WAR: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card, Status.ACTIVATED);
    } else if ((trigger & Trigger.CARD_PLAY) > 0) {
      const turnipCards = [Cards.BAZOOKA_TURNIP, Cards.CENTURION_TURNIP, Cards.ROTNIP,
        Cards.SKULLNIP, Cards.THE_LICHIOUS_TURNIP, Cards.TURNIP, Cards.YOUNG_LICHIOUS_TURNIP
      ];
      const randomTurnipCard = turnipCards[getRandomInt(turnipCards.length)]
      game.logs.push(`Ancient War :`);
      morph(game, data.cardPlayed!, randomTurnipCard, false)
    }
  },
  ANCIENT_WAR_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card);
    } else if ((trigger & Trigger.CARD_PLAY) > 0) {
      const jennyCards = [Cards.APATHETIC_JENNY, Cards.CASUAL_JENNY_BERRY, Cards.CYBER_JENNY, Cards.JENNY_BIRD,
        Cards.JENNY_BIRD_X, Cards.JENNY_BLUEBERRY, Cards.JENNY_BUN, Cards.JENNY_BUNNY, Cards.JENNY_BUNNY_X,
        Cards.JENNY_CAT, Cards.JENNY_DEER, Cards.JENNY_FLOWER, Cards.JENNY_FOX, Cards.JENNY_FROG, Cards.JENNY_LEMON,
        Cards.JENNY_MERMAID, Cards.JENNY_MOLE, Cards.JENNY_SHARK, Cards.JENNY_SLAYER, Cards.JENNY_TIGER,
        Cards.MASKED_JENNY_CAT, Cards.SAFETY_JENNY
      ];
      const randomJennyCard = jennyCards[getRandomInt(jennyCards.length)]
      game.logs.push(`Ancient War X :`);
      morph(game, data.cardPlayed!, randomJennyCard, false)
    }
  },
  ANTI_MURI: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card, 3, true);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 3);
    }
  },
  APATHETIC_FROG: (game: Game, card: CardState, trigger: Trigger, data: Data) => {},

  //2
  APATHETIC_FROG_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0) {
      const apatheticCards = [
        CardsId.APATHETIC_FROG, CardsId.APATHETIC_FROG_X,
        CardsId.APATHETIC_JENNY, CardsId.GOLDEN_APATHETIC_FROG,
      ];

      if (data.cardPlayed!.isBlue == card.isBlue && apatheticCards.includes(data.cardPlayed!.cardId)) {
        const apatheticFrogs = [Cards.APATHETIC_FROG, Cards.APATHETIC_FROG];
        addToDeck(game, card, apatheticFrogs);
      }
    }
  },
  APATHETIC_JENNY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const apatheticFrogs = [Cards.APATHETIC_FROG, Cards.APATHETIC_FROG];
      addToDeck(game, card, apatheticFrogs);
    }
  },
  ASHA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      moveCardRandomly(game, card, data.placementId);
      changeStatus(game, card)
      damage(game, card)
    }
  },
  AYA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      peekAtDeck(game, card, true, 3)
    }
  },
  BAZOOKA_TURNIP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      addShield(game, card)
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  BEAR_MINER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && !data.cardPlayed!.isBlue == game.blueTurn
      && (card.status & Status.ACTIVATED) > 0
    ) {
      moveCardRandomly(game, card, data.placementId)
    }
  },
  BEAUTIFUL_CREATURES: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card, 1, true);
    }
  },
  BEE_SWARM: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card, 1, (game.blueHP < game.redHP) == card.isBlue);
    }
  },

  //3
  BEES: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.CARD_PLAY) > 0 && (card.status & Status.ACTIVATED) > 0) {
      changeStatus(game, data.cardPlayed!, Status.SILENCED);
    }
  },
  BEES_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.CARD_PLAY) > 0 && (card.status & Status.ACTIVATED) > 0) {
      changeStatus(game, data.cardPlayed!, Status.CAGED);
    }
  },
  BENNY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  BIRD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      addToDeck(game, card, [Cards.T_BIRD], true, true)
    }
  },
  BIRD_ARROW: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  BLUEPRINT: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card)
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      addToDeck(game, card, [Cards.T_BLUEPRINT], true)
    }
  },
  BOB_S_SHIP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {},
  BOMB: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.DRAW) > 0) {
      game.logs.push(`Le joueur ${card.isBlue ? "Bleu" : "Rouge"} a pioché Bomb !`);
      damage(game, card, 1, true);
    }
  },

  //4
  BOSS_TRIO: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 3, (game.blueHP > game.redHP) == card.isBlue);
    }
  },
  BRICKOPALYPSE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 4);
    }
  },
  BRUTUS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const modifiedField = makeModifiedField(game.card.field, true);
      const modifiedIndex = data.placementId! + 9 + Math.floor(data.placementId!/6) * 2;
      let freeAdjaccentSpaceCount = 0;
      ArrowTable.forEach(([pointedInd]) => {
        freeAdjaccentSpaceCount += !modifiedField[modifiedIndex + pointedInd] ? 1 : 0;
      });
      damage(game, card, freeAdjaccentSpaceCount)
    }
  },
  BUNBOY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.DRAW) > 0) {
      game.logs.push(`Le joueur ${card.isBlue ? "Bleu" : "Rouge"} a pioché Bunboy !`);
      damage(game, card, 2, (game.blueHP > game.redHP) == card.isBlue);
    }
  },
  BUNI: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      addShield(game, card);
    }
  },
  BUSINESS_CASUAL_MAN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.DAMAGE) > 0
      && (card.status & Status.ACTIVATED) > 0
      && data.toBlue == card.isBlue
    ) {
      damage(game, card);
      return false;
    }
  },
  BUSINESS_CASUAL_MAN__X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.DAMAGE) > 0
      && (card.status & Status.ACTIVATED) > 0
      && data.toBlue == card.isBlue
    ) {
      damage(game, card);
      return false;
    }
  },
  CANDY_SNAKE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {},

  //5
  CARDPLAYA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
  },
  CASUAL_JENNY_BERRY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      const colorHand = card.isBlue ? "blueHand" : "redHand";
      const colorDeck = card.isBlue ? "blueDeck" : "redDeck"
      addToDeck(game, card, game.card[colorHand])
      game.card[colorDeck] = shuffle(game.card[card.isBlue ? "blueDeck" : "redDeck"])
      game.card[colorHand] = game.card[colorDeck].slice(game.card[colorDeck].length - 4, game.card[colorDeck].length);
      game.card[colorDeck] = game.card[colorDeck].slice(0, game.card[colorDeck].length - 4);
      game.logs.push(`Casual Jenny Berry : Le joueur ${card.isBlue ? "Bleu" : "Rouge"
      } a remis sa main dans son deck, l'a mélangé et a repioché 4 cartes`);
    }
  },
  CAUTION: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
  },
  CENTURION_TURNIP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    } else if ((trigger & Trigger.CARD_PLAY) > 0 && data.cardPlayed!.isBlue == card.isBlue) {
      const turnipCards = [CardsId.BAZOOKA_TURNIP, CardsId.CENTURION_TURNIP, CardsId.ROTNIP,
        CardsId.SKULLNIP, CardsId.THE_LICHIOUS_TURNIP, CardsId.TURNIP, CardsId.YOUNG_LICHIOUS_TURNIP
      ];
      if (turnipCards.includes(data.cardPlayed!.cardId)) {
        morph(game, card, Cards.BAZOOKA_TURNIP);
      }
    }
  },
  CHILLY_ROGER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card);
    }
  },
  COGS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      addToDeck(game, card, [Cards[CardsId[getRandomInt(CardsId.Z + 1)]]], true, true);
    }
  },
  CORRUPTION: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  CROSSOVER_ADVENTURE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
      heal(game, card);
      damage(game, card, 1, true);
      addShield(game, card);
    }
  },

  //6
  CRUISER_TETRON: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  CYBER_JENNY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.DRAW) > 0) {
      game.logs.push(`Le joueur ${card.isBlue ? "Bleu" : "Rouge"} a pioché Cyber Jenny !`);
      damage(game, card);
    } else if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  CYBER_SKULL: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2, true);
      damage(game, card, 2);
    }
  },
  DANCE_PARTY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {

    }
  },
  DARK_LORD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {

    }
  },
  DARK_LORD_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {

    }
  },
  DARK_RUBY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const didPopShield = popShield(game, card);
      if (didPopShield) {
        damage(game, card);
        addShield(game, card);
      }
    }
  },
  DEA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      const colorToken = card.isBlue ? "blueToken" : "redToken";
      if (game[colorToken].MANA > 0) {
        game[colorToken].MANA--;
        morph(game, card, Cards.NEO_DEA);
        peekAtHand(game, card);
      }
    }
  },

  //7
  DEATHBLOW: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const didPopShield = popShield(game, card) && popShield(game, card, true);
      if (didPopShield) {
        damage(game, card, 5);
      }
    }
  },
  DENTIST_MAZE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.CARD_PLAY) > 0 && !data.cardPlayed!.isBlue == card.isBlue) {
      moveCardRandomly(game, data.cardPlayed!, data.cardPlayedId!)
      changeStatus(game, card)
    }
  },
  DEVASOM: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {

    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  DEVIL_REMEDY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      const ownColorToken = card.isBlue ? "blueToken" : "redToken";
      const opponentColorToken = !card.isBlue ? "blueToken" : "redToken";
      if (game[opponentColorToken].MANA > 0) {
        game[opponentColorToken].MANA--;
        game[ownColorToken].MANA++;
        game.logs.push(`Le joueur ${card.isBlue ? "Bleu" : "Rouge"} a volé un jeton Mana !`);
      }
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
      heal(game, card);
    }
  },
  DISILLUSIONED_SNAKE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      game.card.field.forEach(fieldCard => {
        fieldCard && (fieldCard.status & Status.ACTIVATED) > 0 && fieldCard.delay < 9 && fieldCard.delay > 0
        ? fieldCard.delay++
        : null
      })
      game.logs.push(`Desillusioned Snake : Toutes les cartes actives ont été retardées`);
    }
  },
  DOWNHILL_DUCK: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      game.card.field.forEach(fieldCard => {
        fieldCard && (fieldCard.status & Status.ACTIVATED) > 0 && fieldCard.delay > 1
        ? fieldCard.delay--
        : null
      })
      game.logs.push(`Downhill Duck : Toutes les cartes actives ont été avancées`);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 1, true);
    }
  },
  DREAM_ROCK: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.DRAW) > 0) {
      const ownColorToken = card.isBlue ? "blueToken" : "redToken";
      if (game[ownColorToken].MANA >= 2) {
        game[ownColorToken].MANA++
      }
    }
  },
  DRONE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },

  //8
  DUCKLORD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  DUMPED: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {

    }
  },
  DUMPED_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {

    }
  },
  DYNAMITE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card)
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
      damage(game, card, 1, true);
    }
  },
  EVIL_EASEL: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {

    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 1, (game.blueHP > game.redHP) == card.isBlue);
    }
  },
  FAE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && (card.status & Status.ACTIVATED) > 0) {
      const tippsieCards = [CardsId.TIPPSIE, CardsId.TIPPSIE_TOSS];
      if (tippsieCards.includes(data.cardPlayed!.cardId)) {
        game.card.field[data.cardPlayedId!] = null;
        game.logs.push(`Fae : ${data.cardPlayed!.name} a été enlevé.`);
        heal(game, card, 2);
      }
    }
  },
  FASEN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addToDeck(game, card, [Cards.POTATO, Cards.POTATO], true)
    }
  },
  FEAR_OF_BIRDS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && (card.status & Status.ACTIVATED) > 0
      && !card.isBlue == data.cardPlayed!.isBlue
    ) {
      const birdCards = [CardsId.BIRD, CardsId.BIRD_ARROW, CardsId.FEAR_OF_BIRDS, CardsId.JENNY_BIRD,
        CardsId.JENNY_BIRD_X, CardsId.NETHERBIRD, CardsId.T_BIRD];
      if (birdCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card, 2, true);
      }
    }
  },

  //9
  FERAL_GATE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      const modifiedField = makeModifiedField(game.card.field, null);
      const modifiedIndex = data.placementId! + 9 + Math.floor(data.placementId!/6) * 2;
      const adjacentCards : Array<CardState> = [];
      ArrowTable.forEach(([pointedInd]) => {
        const pointedCard = modifiedField[modifiedIndex + pointedInd];
        pointedCard ? adjacentCards.push(pointedCard) : null;
      });
      if (adjacentCards.length > 0)
        changeStatus(game, adjacentCards[getRandomInt(adjacentCards.length)], Status.CAGED)
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      const modifiedField = makeModifiedField(game.card.field, null);
      const modifiedIndex = data.placementId! + 9 + Math.floor(data.placementId!/6) * 2;
      const adjacentCards : Array<CardState> = [];
      ArrowTable.forEach(([pointedInd]) => {
        const pointedCard = modifiedField[modifiedIndex + pointedInd];
        pointedCard ? adjacentCards.push(pointedCard) : null;
      });
      if (adjacentCards.length > 0)
        changeStatus(game, adjacentCards[getRandomInt(adjacentCards.length)], Status.NONE, true)
    }
  },
  FINAL_BRUTUS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      popShield(game, card);
      damage(game, card, 2);
    }
  },
  FIRE_MACE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  FIRE_SWORD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  FISHBUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0) {
      const bunCards = [CardsId.BUNBOY, CardsId.FISHBUN, CardsId.JENNY_BUN, CardsId.LONGBUN,
        CardsId.PRIMAL_FISHBUN, CardsId.SHELLBUN, CardsId.SPIKEBUN, CardsId.ULTRA_FISHBUNJIN_3000,
        CardsId.ULTRA_MECHABUN];
      if (bunCards.includes(data.cardPlayed!.cardId)) {
        game.card.field[data.cardPlayedId!] = null;
        game.logs.push(`Fishbun : ${data.cardPlayed!.name} a été enlevé.`);
        morph(game, card, Cards.FISHBUN_PILEUP)
      }
    }
  },
  FLIP_HERO: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  FORCE_WAND: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      const ownColorToken = card.isBlue ? "blueToken" : "redToken";
      game[ownColorToken].MANA++;
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      const ownColorToken = card.isBlue ? "blueToken" : "redToken";
      damage(game, card, Math.floor(game[ownColorToken].MANA / 2));
    }
  },
  FRALLAN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.HEAL) > 0
      && (card.status & Status.ACTIVATED) > 0
      && data.toBlue == card.isBlue
      && data.amount == 1
    ) {
      heal(game, card, 2);
      return false;
    }
  },

  //10
  FRIEND: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  FROG_FAN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  GARDEN_GNOME: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    }
  },
  GODDESS_BUSTER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    } else if ((trigger & Trigger.CARD_PLAY) > 0
      && data.cardPlayed!.isBlue == card.isBlue
      && data.cardPlayed!.cardId == CardsId.GODDESS_OF_EXPLOSIONS
    ) {
      damage(game, card, 2);
      game.card.field[data.placementId!] = null
    }
  },
  GODDESS_OF_EXPLOSIONS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card, 1, true);
      damage(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 1, true);
      damage(game, card);
    }
  },
  GOLDEN_APATHETIC_FROG: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && (card.status & Status.ACTIVATED) > 0
      && !card.isBlue == data.cardPlayed!.isBlue
      && data.cardPlayed!.cardId == CardsId.APATHETIC_FROG
    ) {
      game[card.isBlue ? "redHP" : "blueHP"] = -99
    }
  },
  GUNNEL_VISION: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && card.isBlue == data.cardPlayed!.isBlue
    ) {
      const wandCards = [CardsId.FORCE_WAND, CardsId.ICE_WAND, CardsId.PORTAL_WAND];
      if (wandCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card)
      }
    }
  },
  HAJA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card, 1, true);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },

  //11
  HANDS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  HEART: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.DRAW) > 0) {
      game.logs.push(`Le joueur ${card.isBlue ? "Bleu" : "Rouge"} a pioché Heart !`);
      heal(game, card);
    }
  },
  HEDVIG: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  HELIOS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && (card.status & Status.ACTIVATED) > 0
      && !card.isBlue == data.cardPlayed!.isBlue
    ) {
      damage(game, card);
    }
  },
  HERMITLEGS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const cardsToMove : Array<CardState> = [];
      const temporaryField = game.card.field.map((cardField) => {
         if (cardField && cardField.delay > 0) {
           cardsToMove.push(cardField);
           return null;
         } else {
           return cardField;
         }
      });
      game.card.field = temporaryField;
      cardsToMove.forEach((cardToMove) => moveCardRandomly(game, cardToMove, -1));
    }
  },
  HEROISM: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && !card.isBlue == data.cardPlayed!.isBlue) {
      const jennyCards = [CardsId.APATHETIC_JENNY, CardsId.CASUAL_JENNY_BERRY, CardsId.CYBER_JENNY, CardsId.JENNY_BIRD,
        CardsId.JENNY_BIRD_X, CardsId.JENNY_BLUEBERRY, CardsId.JENNY_BUN, CardsId.JENNY_BUNNY, CardsId.JENNY_BUNNY_X,
        CardsId.JENNY_CAT, CardsId.JENNY_DEER, CardsId.JENNY_FLOWER, CardsId.JENNY_FOX, CardsId.JENNY_FROG, CardsId.JENNY_LEMON,
        CardsId.JENNY_MERMAID, CardsId.JENNY_MOLE, CardsId.JENNY_SHARK, CardsId.JENNY_SLAYER, CardsId.JENNY_TIGER,
        CardsId.MASKED_JENNY_CAT, CardsId.SAFETY_JENNY
      ];
      if (jennyCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card);
        changeStatus(game, card, Status.SILENCED);
      }
    }
  },
  HEROISM_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && !card.isBlue == data.cardPlayed!.isBlue) {
      const ittleCards = [CardsId.ITTLE_DEW, CardsId.ITTLE_DEW_TOO, CardsId.OLD_ITTLE];
      if (ittleCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card);
        changeStatus(game, card, Status.SILENCED);
      }
    }
  },
  HEXROT: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, game[card.isBlue ? "blueToken" : "redToken"].MANA)
    }
  },

  //12
  HEXROT_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, game[card.isBlue ? "blueToken" : "redToken"].MANA, true)
    }
  },
  HORSEGUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addToDeck(game, card, [Cards.T_HORSEGUN], true);
    } else if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      const horseCards = [CardsId.HORSEGUN, CardsId.T_HORSEGUN];
      if (horseCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card);
      }
    }
  },
  HYPE_SNAKE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0) {
      moveCardRandomly(game, card, data.placementId!);
      changeStatus(game, card);
    }
  },
  HYPERDUSA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card, 1, (game.blueHP > game.redHP) == card.isBlue);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  ICE_RING: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      
    }
  },
  ICE_WAND: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  IDUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.CARD_PLAY) > 0
      && card.isBlue == data.cardPlayed!.isBlue
      && data.cardPlayed!.cardId == CardsId.IDUN
    ) {
      
    }
  },
  IJI: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      addShield(game, card, false);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      addShield(game, card);
    }
  },

  //13
  INCREDIBLY_UGLY_STATUES: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
  },
  IOSA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  ITAN_CARVER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      addToDeck(game, card, [Cards.BOMB]);
    }
  },
  ITTLE_DEW: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
      damage(game, card, 1, true);
    }
  },
  ITTLE_DEW_TOO: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  JAM: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card, 2);
    }
  },
  JEALOUS_CHEST: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      game[card.isBlue ? "blueToken" : "redToken"].POWER += 1;
    }
  },
  JELLY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      const modifiedField = makeModifiedField(game.card.field, null);
      const modifiedIndex = data.placementId! + 9 + Math.floor(data.placementId!/6) * 2;
      const adjacentCards : Array<CardState> = [];
      ArrowTable.forEach(([pointedInd]) => {
        const pointedCard = modifiedField[modifiedIndex + pointedInd];
        pointedCard ? adjacentCards.push(pointedCard) : null;
      });
    }
  },

  //14
  JENNY_BIRD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      const birdCards = [CardsId.BIRD, CardsId.BIRD_ARROW, CardsId.FEAR_OF_BIRDS,
        CardsId.JENNY_BIRD, CardsId.JENNY_BIRD_X, CardsId.NETHERBIRD, CardsId.T_BIRD];
      if (birdCards.includes(data.cardPlayed!.cardId)) {
        morph(game, data.cardPlayed!, Cards.BIRD_ARROW);
        changeStatus(game, data.cardPlayed!);
      }
    }
  },
  JENNY_BIRD_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && card.isBlue == data.cardPlayed!.isBlue
      && data.cardPlayed!.cardId == CardsId.BIRD_ARROW
    ) {
        morph(game, data.cardPlayed!, Cards.BIRD);
        changeStatus(game, data.cardPlayed!);
    }
  },
  JENNY_BLUEBERRY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
      damage(game, card, 1, true);
    }
  },
  JENNY_BUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && (card.status & Status.ACTIVATED) > 0) {
      const bunCards = [CardsId.BUNBOY, CardsId.FISHBUN, CardsId.JENNY_BUN, CardsId.LONGBUN,
        CardsId.PRIMAL_FISHBUN, CardsId.SHELLBUN, CardsId.SPIKEBUN, CardsId.ULTRA_FISHBUNJIN_3000,
        CardsId.ULTRA_MECHABUN];
      if (bunCards.includes(data.cardPlayed!.cardId)) {
        game.card.field[data.cardPlayedId!] = null;
        game.logs.push(`Jenny Bun : ${data.cardPlayed!.name} a été enlevé.`);
        addShield(game, card);
      }
    }
  },
  JENNY_BUNNY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && !card.isBlue == data.cardPlayed!.isBlue) {
      game.card.field[data.cardPlayedId!] = null;
      game.logs.push(`Jenny Bunny : ${data.cardPlayed!.name} a été enlevé.`);
      changeStatus(game, card, Status.SILENCED);
    }
  },
  JENNY_BUNNY_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && !card.isBlue == data.cardPlayed!.isBlue) {
      data.cardPlayed!.isBlue = !data.cardPlayed!.isBlue;
      game.logs.push(`Jenny Bunny X : ${data.cardPlayed!.name} a changé de couleur.`);
      changeStatus(game, card, Status.SILENCED);
    }
  },
  JENNY_CAT: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  JENNY_DEER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const ownColorToken = card.isBlue ? "blueToken" : "redToken";;
      game[ownColorToken].MANA -= Math.min(3, game[ownColorToken].MANA);
      if (game[ownColorToken].MANA > 0) damage(game, card, game[ownColorToken].MANA);
    }
  },

  //15
  JENNY_FLOWER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      heal(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  JENNY_FOX: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card);
    }
  },
  JENNY_FROG: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  JENNY_LEMON: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && (card.status & Status.ACTIVATED) > 0) {
      const jennyCards = [CardsId.APATHETIC_JENNY, CardsId.CASUAL_JENNY_BERRY, CardsId.CYBER_JENNY, CardsId.JENNY_BIRD,
        CardsId.JENNY_BIRD_X, CardsId.JENNY_BLUEBERRY, CardsId.JENNY_BUN, CardsId.JENNY_BUNNY, CardsId.JENNY_BUNNY_X,
        CardsId.JENNY_CAT, CardsId.JENNY_DEER, CardsId.JENNY_FLOWER, CardsId.JENNY_FOX, CardsId.JENNY_FROG, CardsId.JENNY_LEMON,
        CardsId.JENNY_MERMAID, CardsId.JENNY_MOLE, CardsId.JENNY_SHARK, CardsId.JENNY_SLAYER, CardsId.JENNY_TIGER,
        CardsId.MASKED_JENNY_CAT, CardsId.SAFETY_JENNY
      ];
      if (jennyCards.includes(data.cardPlayed!.cardId)) {
        game.card.field[data.cardPlayedId!] = null;
        game.logs.push(`Jenny Lemon : ${data.cardPlayed!.name} a été enlevé.`);
        damage(game, card);
      }
    }
  },
  JENNY_MERMAID: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  JENNY_MOLE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  JENNY_SHARK: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  JENNY_SLAYER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      const jennyCards = [CardsId.APATHETIC_JENNY, CardsId.CASUAL_JENNY_BERRY, CardsId.CYBER_JENNY, CardsId.JENNY_BIRD,
        CardsId.JENNY_BIRD_X, CardsId.JENNY_BLUEBERRY, CardsId.JENNY_BUN, CardsId.JENNY_BUNNY, CardsId.JENNY_BUNNY_X,
        CardsId.JENNY_CAT, CardsId.JENNY_DEER, CardsId.JENNY_FLOWER, CardsId.JENNY_FOX, CardsId.JENNY_FROG, CardsId.JENNY_LEMON,
        CardsId.JENNY_MERMAID, CardsId.JENNY_MOLE, CardsId.JENNY_SHARK, CardsId.JENNY_SLAYER, CardsId.JENNY_TIGER,
        CardsId.MASKED_JENNY_CAT, CardsId.SAFETY_JENNY
      ];
      if (jennyCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card, 1, true);
      }
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 3);
    }
  },

  //16
  JENNY_TIGER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.CARD_PLAY) > 0
      && card.isBlue == data.cardPlayed!.isBlue
      && (card.status & Status.ACTIVATED) > 0
      && data.cardPlayed!.cardId == CardsId.ROTNIP) {
      changeStatus(game, data.cardPlayed!, Status.CAGED);
      changeStatus(game, data.cardPlayed!);
      if (card.delay < 9) card.delay++;
      damage(game, card);
    }
  },
  KEY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  KEY_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      changeStatus(game, data.cardPlayed!)
      changeStatus(game, card)
    }
  },
  KILLER_KARP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  KING_BUNI: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  KNUCKLER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2, (game.blueHP > game.redHP) == card.isBlue);
    }
  },
  KROTERA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  LE_BIADLO: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },

  //17
  LEAN_BOO: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && !card.isBlue == data.cardPlayed!.isBlue) {
      changeStatus(game, data.cardPlayed!, Status.CAGED)
      changeStatus(game, data.cardPlayed!, Status.SILENCED)
      changeStatus(game, data.cardPlayed!)
      game.card.field[data.placementId!] == null
      game.logs.push(`Lean Boo : s'enlève d'elle même.`);
    }
  },
  LEMON: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && card.isBlue == data.cardPlayed!.isBlue
      && (card.status & Status.ACTIVATED) > 0
    ) {
      data.cardPlayed!.isBlue = !data.cardPlayed!.isBlue;
      game.logs.push(`Jenny Bunny X : ${data.cardPlayed!.name} a changé de couleur.`);
    }
  },
  LENNY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      game.card.field.forEach(fieldCard => {
        fieldCard && (fieldCard.status & Status.ACTIVATED) > 0 && fieldCard.delay < 9 && fieldCard.delay > 0
        ? fieldCard.delay++
        : null
      });
      game.logs.push(`Lenny : Toutes les cartes actives ont été retardées.`);
    }
  },
  LIFE_PRESERVER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addShield(game, card);
    }
  },
  LIFE_PRESERVER_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addShield(game, card, false);
    }
  },
  LONGBUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {},
  MAGIC_CRYSTAL: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const ownColorToken = card.isBlue ? "blueToken" : "redToken";
      game[ownColorToken].MANA++;
    }
  },
  MAMA_AND_SON: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },

  //18
  MANBUNJIN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const didPopShield = popShield(game, card);
      if (didPopShield) {
        const ownColorToken = card.isBlue ? "blueToken" : "redToken";
        game[ownColorToken].POWER++
      }
    }
  },
  MAPMAN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  MARIANNE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  MARK: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  MASKED_JENNY_CAT: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const modifiedField = makeModifiedField(game.card.field, null);
      const modifiedIndex = data.placementId! + 9 + Math.floor(data.placementId!/6) * 2;
      const group : Array<CardState> = [];
      getConnectedGroup(card, data.placementId!, group, [], modifiedField);
      let connectedCardCount = 0;
      ArrowTable.forEach(([pointedInd]) => {
        const pointedCard = modifiedField[modifiedIndex + pointedInd];
        pointedCard && group.includes(pointedCard) ? connectedCardCount++ : null;
      });
      damage(game, card, connectedCardCount);
    }
  },
  MASKED_RUBY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addShield(game, card);
      damage(game, card);
    }
  },
  MECHA_SANTA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  MECHA_SANTA_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 1, true);
    } else if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      const mechaCards = [CardsId.MECHA_SANTA, CardsId.MECHA_SANTA_X, CardsId.ULTRA_MECHABUN];
      if (mechaCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card);
        card.isBlue = !card.isBlue;
      }
    }
  },

  //19
  METONYM: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card, 2);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 3, true);
    }
  },
  MOOSE_MAN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  MOTOR_MAN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const ownColorToken = card.isBlue ? "blueToken" : "redToken";
      damage(game, card, Math.floor(game[ownColorToken].MANA/2), (game.blueHP > game.redHP) == card.isBlue);
    }
  },
  MR_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 3);
    }
  },
  NEON: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      peekAtDeck(game, card);
      peekAtDeck(game, card, true);
    }
  },
  NETHERBIRD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 1, true);
    }
  },
  NILS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addShield(game, card);
      damage(game, card, 2, (game.blueHP > game.redHP) == card.isBlue);
    }
  },
  NUNA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card);
    }
  },

  //20
  OCTACLE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const opponentHand = game.card[card.isBlue ? "redHand" : "blueHand"];
      opponentHand[getRandomInt(opponentHand.length)] = copyCard(Cards.SAD_TRASH)
      game.logs.push(`Octacle : une carte de la main du joueur ${
        card.isBlue ? "Rouge" : "Bleu"} a été remplacé par Sad trash`);
    }
  },
  OH: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card);
    }
  },
  OLD_ITTLE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const rand: number = getRandomInt(4);
      if (rand === 0) {
        damage(game, card, 2, true);
      } else {
        damage(game, card, rand);
      }
    }
  },
  OOGLER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  ORKA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      heal(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2, (game.blueHP > game.redHP) == card.isBlue);
    }
  },
  PARANOID_FISH: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0) {
      if (card.isBlue == data.cardPlayed!.isBlue) {
        damage(game, card, 1, true);
      } else {
        damage(game, card);
      }
    }
  },
  PARROT: (game: Game, card: CardState, trigger: Trigger, data: Data) => {},
  PARROT_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && !card.isBlue == data.cardPlayed!.isBlue) {
      morph(game, card, data.cardPlayed!, true);
    }
  },

  //21
  PASSEL: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      changeStatus(game, data.cardPlayed!);
      if (data.cardPlayed!.delay < 9) data.cardPlayed!.delay++;
      if (data.cardPlayed!.delay < 9) data.cardPlayed!.delay++;
      changeStatus(game, card);
    }
  },
  PETAL_SLUG: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      popShield(game, card);
      damage(game, card);
    }
  },
  PITCH_AND_CATSTRIKE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  PLASMA_HYDRA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card, 3, true);
      damage(game, card, 2);
    }
  },
  PORTAL_WAND: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      
    }
  },
  PORTAL_WORLD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      
    }
  },
  POTATO: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.DRAW) > 0) {
      game.logs.push(`Le joueur ${card.isBlue ? "Bleu" : "Rouge"} a pioché Potato !`);
      damage(game, card);
    }
  },
  PRIMAL_FISHBUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && (card.status & Status.ACTIVATED) > 0) {
      const bunCards = [CardsId.BUNBOY, CardsId.FISHBUN, CardsId.JENNY_BUN, CardsId.LONGBUN,
        CardsId.PRIMAL_FISHBUN, CardsId.SHELLBUN, CardsId.SPIKEBUN, CardsId.ULTRA_FISHBUNJIN_3000,
        CardsId.ULTRA_MECHABUN];
      if (bunCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card);
      }
    }
  },

  //22
  PRINCE_HINGST: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 3, true)
      damage(game, card, 2, true)
      damage(game, card, 1, true)
    }
  },
  PRINCESS_REMEDY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && (card.status & Status.ACTIVATED) > 0
      && !card.isBlue == data.cardPlayed!.isBlue
    ) {
      heal(game, card)
    }
  },
  PSICLOPS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && (card.status & Status.ACTIVATED) > 0) {
      damage(game, card, 1, (game.blueHP > game.redHP) == card.isBlue);
    }
  },
  PUZZLE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      game.card.field.forEach((cardField) =>
        cardField && !card.isBlue == cardField.isBlue && cardField.delay > 0
        && cardField.delay < 9 && (cardField.status & Status.ACTIVATED) > 0
        ? cardField.delay++
        : null
      )
    }
  },
  QUEEN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      const kingCards = [CardsId.KING_BUNI, CardsId.THE_KING, CardsId.SKULL_KING]
      if (kingCards.includes(data.cardPlayed!.cardId)) {
        
        changeStatus(game, card)
      }
    }
  },
  QUEEN_AMELIA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {

      const colorToken = card.isBlue ? "blueToken" : "redToken";
      game[colorToken].MANA += 2
    }
  },
  QUEEN_AMELIA_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {

      const colorToken = card.isBlue ? "blueToken" : "redToken";
      game[colorToken].MANA -= Math.min(2, game[colorToken].MANA)
    }
  },
  RAFT: (game: Game, card: CardState, trigger: Trigger, data: Data) => {},

  //23
  RAINBOW_RING: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      addToDeck(game, card, [data.cardPlayed!], true, true)
      game.card.field[data.placementId!] = null;
    }
  },
  RANDO: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  RAZZLE_DAZZLE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  REIKA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);

    }
  },
  ROLE_REVERSAL: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const redHand = game.card.redHand;
      const switchColor = (cardHand : CardState) => {
        return {...cardHand, isBlue: !cardHand.isBlue, isArrowBlue: !cardHand.isArrowBlue}
      }
      game.card.redHand = game.card.blueHand.map(switchColor);
      game.card.blueHand = redHand.map(switchColor);
      game.logs.push(`Role Reversal : Les mains ont été échangées`);
    }
  },
  ROLL: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addShield(game, card);
    }
  },
  ROTNIP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addShield(game, card);
    }
  },
  SAFETY_JENNY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {},

  //24
  SAM: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      const colorToken = card.isBlue ? "blueToken" : "redToken";
      if (game[colorToken].MANA > 0) {
        game[colorToken].MANA--;
        damage(game, card);
      }
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      peekAtDeck(game, card);
    }
  },
  SATURNUS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      heal(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      peekAtDeck(game, card, true);
    }
  },
  SHELLBUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  SHOP_AT_ITAN_S: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addToDeck(game, card, [Cards.PORTAL_WAND, Cards.ICE_WAND, Cards.FORCE_WAND], true);
    }
  },
  SHORTCUT: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const didPopShield = popShield(game, card);
      if (didPopShield) damage(game, card, 2);
    }
  },
  SIMULACRUM: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.DAMAGE) > 0 && !data.toBlue == card.isBlue && data.amount == 2) {
      damage(game, card, 3);
      return false;
    }
  },
  SIMULACRUM_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.DAMAGE) > 0 && data.toBlue == card.isBlue && data.amount == 3) {
      addShield(game, card);
      return false;
    }
  },
  SKULL_CARD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card, 2, true);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },

  //25
  SKULL_CARD_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && (card.status & Status.ACTIVATED) > 0
      && card.isBlue == data.cardPlayed!.isBlue
    ) {
      damage(game, card, 1, true);
    }
  },
  SKULL_KING: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  SKULLNIP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const modifiedField = makeModifiedField(game.card.field, null);
      const modifiedIndex = data.placementId! + 9 + Math.floor(data.placementId!/6) * 2;
      const group : Array<CardState> = [];
      getConnectedGroup(card, data.placementId!, group, [], modifiedField);
      let connectedTurnipCardCount = 0;
      const turnipCards = [CardsId.BAZOOKA_TURNIP, CardsId.CENTURION_TURNIP, CardsId.ROTNIP,
        CardsId.SKULLNIP, CardsId.THE_LICHIOUS_TURNIP, CardsId.TURNIP, CardsId.YOUNG_LICHIOUS_TURNIP
      ];
      ArrowTable.forEach(([pointedInd]) => {
        const pointedCard = modifiedField[modifiedIndex + pointedInd];
        pointedCard && group.includes(pointedCard) && turnipCards.includes(pointedCard.cardId)
        ? connectedTurnipCardCount++
        : null;
      });
      damage(game, card, connectedTurnipCardCount);
    }
  },
  SLIME: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card);
    }
  },
  SLUG: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {

    }
  },
  SNAP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  SOFIA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {

    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  SPACE_NORD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.SHIELD) > 0 && !data.toBlue == card.isBlue && (card.status & Status.ACTIVATED) > 0) {
      addShield(game, card);
    }
  },

  //26
  SPACE_NORD_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.SHIELD) > 0 && data.toBlue == card.isBlue && (card.status & Status.ACTIVATED) > 0) {
      addShield(game, card, false);
    }
  },
  SPECTRE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      const colorToken = card.isBlue ? "blueToken" : "redToken";
      game[colorToken].MANA++
    } else if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      const colorToken = card.isBlue ? "blueToken" : "redToken";
      if (game[colorToken].MANA > 0) {
        game[colorToken].MANA--
        damage(game, card);
      }
    }
  },
  SPIKE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  SPIKE_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      popShield(game, card);
      damage(game, card);
    }
  },
  SPIKEBUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.SHIELD) > 0 && data.toBlue == card.isBlue) {
      damage(game, card, 2);
      return false
    }
  },
  STAR_SPLITTER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {

    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card)
    }
  },
  STICK: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card)
    }
  },
  STRANGLE_SWAN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },

  //27
  STUPID_BEE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  SUPER_ANN_ZEIGER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card);
    }
  },
  SWIMMY_ROGER: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 1, Boolean(getRandomInt(2)));
      damage(game, card, 1, Boolean(getRandomInt(2)));
      damage(game, card, 1, Boolean(getRandomInt(2)));
    }
  },
  THAT_GUY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      heal(game, card, 2, (game.blueHP < game.redHP) == card.isBlue);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 3);
    }
  },
  THAT_GUY_WITH_HANDS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0
      && (card.status & Status.ACTIVATED) > 0
      && card.isBlue == data.cardPlayed!.isBlue
    ) {
      game.card.field.forEach((cardField, ind) => 
        cardField && !(ind == data.cardPlayedId) ? switchColor(game, cardField) : null
      )
    }
  },
  THE_BIGGEST_FAN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card)
    }
  },
  THE_KING: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      const qweenCards = [CardsId.QUEEN, CardsId.QUEEN_AMELIA, CardsId.QUEEN_AMELIA_X];
      if (qweenCards.includes(data.cardPlayed!.cardId)) {
        game.card.field.forEach((cardField, ind) => 
          cardField ? changeStatus(game, cardField) : null
        )
      }
    }
  },
  THE_LICHIOUS_TURNIP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {

    }
  },

  //28
  TIPPSIE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card);
    }
  },
  TIPPSIE_TOSS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && (card.status & Status.ACTIVATED) > 0) {
      const tippsieCards = [CardsId.TIPPSIE, CardsId.TIPPSIE_TOSS];
      if (tippsieCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card);
      }
    }
  },
  TITAN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && !card.isBlue == data.cardPlayed!.isBlue) {
      moveCardRandomly(game, card, data.placementId)
    }
  },
  TOR: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {

    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2)
    }
  },
  TOSCA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      addToDeck(game, card, [Cards.KEY, Cards.KEY], true);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card, 3);
    }
  },
  TOSCA_X: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card);
    } else if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      changeStatus(game, card, Status.CAGED);
      addShield(game, card);
    }
  },
  TURNIP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      addToDeck(game, card, [Cards.ROTNIP], true, true);
    }
  },
  ULTRA_FISHBUNJIN_3000: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.ACTIVATE) > 0) {
      changeStatus(game, card, Status.NON_CONDUCTIVE);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 4);
    } else if ((trigger & Trigger.CARD_PLAY) > 0
      && card.isBlue == data.cardPlayed!.isBlue
      && data.cardPlayed!.base == Base.COMMON
    ) {
      if (card.delay > 1) card.delay--
      game.logs.push(`Ultra Fishbun 3000 : Cette carte a été avancée`);
    }
  },

  //29
  ULTRA_MECHABUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2)
    } else if ((trigger & Trigger.CARD_PLAY) > 0
      && (card.status & Status.ACTIVATED) > 0
      && card.isBlue == data.cardPlayed!.isBlue
    ) {
      morph(game, data.cardPlayed!, Cards.FISHBUN);
    }
  },
  UR: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      peekAtHand(game, card);
    }
  },
  VAL_ENBERG: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {

    }
  },
  VALENTINE_LENNY: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card, 5, (game.blueHP < game.redHP) == card.isBlue);
    }
  },
  VOLCANO: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      const modifiedField = makeModifiedField(game.card.field, null);
      const modifiedIndex = data.placementId! + 9 + Math.floor(data.placementId!/6) * 2;
      const adjacentCards : Array<CardState> = [];
      ArrowTable.forEach(([pointedInd]) => {
        const pointedCard = modifiedField[modifiedIndex + pointedInd];
        pointedCard ? adjacentCards.push(pointedCard) : null;
      });
      changeStatus(game, adjacentCards[getRandomInt(adjacentCards.length)], Status.SILENCED);
    }
  },
  WASH_OLOF: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 3);
    }
  },
  YOUNG_LICHIOUS_TURNIP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card);
    } else if ((trigger & Trigger.CARD_PLAY) > 0 && !card.isBlue == data.cardPlayed!.isBlue) {
      const colorToken = card.isBlue ? "blueToken" : "redToken";
      game[colorToken].MANA++
    }
  },
  Z: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      damage(game, card, 1, true);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },

  // Unobtainable (Not available in deck editing)
  A_LEMON: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      
    }
  },
  ANCESTRAL_DUCKLORD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {

    } else if ((trigger & Trigger.CARD_PLAY) > 0
      && (card.status & Status.ACTIVATED) > 0
      && !card.isBlue == data.cardPlayed!.isBlue
    ) {
      popShield(game, card);
      damage(game, card);
    }
  },
  BUNICORN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {
      popShield(game, card);
      damage(game, card, 2);
      addToDeck(game, card, [Cards.HEART], true);
    }
  },
  DEATH_THROE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && (card.status & Status.ACTIVATED) > 0) {
      heal(game, card);
      addShield(game, card);
    }
  },
  LEMON_COCKTAIL: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.RESOLVE) > 0) {

    }
  },
  LEMON_JUICE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },
  LEMON_TART: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card);
    }
  },
  LEMONS: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card);
    } else if ((trigger & Trigger.RESOLVE) > 0) {
      const lemonCards = [Cards.A_LEMON, Cards.LEMON_COCKTAIL, Cards.LEMON_JUICE,
        Cards.LEMON_TART, Cards.SLICED_LEMON]
      const lemonDeck : Array<CardState> = Array(0)
        .concat(
          ...lemonCards.map((lemonCard) => Array(10).fill(lemonCard))
        )
      const shuffledLemonDeck = shuffle(lemonDeck)
      game.card.blueDeck = shuffledLemonDeck.slice(0, 25);
      game.card.redDeck = shuffledLemonDeck.slice(25).map((redCard) => {
        return {...redCard, isBlue: false, isArrowBlue: redCard.isArrowBlue == !redCard.isArrowBlue}
      });

      game.card.blueHand = game.card.blueDeck.splice(game.card.blueDeck.length - 6, 5);
      game.card.redHand = game.card.redDeck.splice(game.card.redDeck.length - 6, 5);

      const cardsToMove : Array<CardState> = [];
      const temporaryField = game.card.field.map((cardField) => {
        if (cardField) {
          morph(game, cardField, lemonCards[getRandomInt(lemonCards.length)], !(cardField.delay > 0), false);
          return cardField;
        } else {
          return null;
        }
      });
      game.card.field = temporaryField;
      cardsToMove.forEach((cardToMove) => moveCardRandomly(game, cardToMove, -1));

      game.logs.push(`Lemons : La limonade est prête ;)`);
    }
  },
  SLICED_LEMON: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 2);
    }
  },

  // Tokens
  T_BIRD: (game: Game, card: CardState, trigger: Trigger, data: Data) => {},
  T_BLUEPRINT: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.PLACEMENT) > 0) {
      changeStatus(game, card);
    }
  },
  FATIGUE: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.DRAW) > 0) {
      const colorToken = card.isBlue ? "blueToken" : "redToken";
      game[colorToken].FATIGUE++
      damage(game, card, game[colorToken].FATIGUE, true)
    }
  },
  FISHBUN_PILEUP: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      damage(game, card, 3);
    }
  },
  T_HORSEGUN: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.CARD_PLAY) > 0 && card.isBlue == data.cardPlayed!.isBlue) {
      const horseCards = [CardsId.HORSEGUN, CardsId.T_HORSEGUN];
      if (horseCards.includes(data.cardPlayed!.cardId)) {
        damage(game, card);
      }
    }
  },
  NEO_DEA: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    if ((trigger & Trigger.RESOLVE) > 0) {
      heal(game, card, 2);
    }
  },
  SAD_TRASH: (game: Game, card: CardState, trigger: Trigger, data: Data) => {
    //UNFINISHED
    if ((trigger & Trigger.PLACEMENT) > 0) {

      changeStatus(game, card);
    }
  },
};

export default CardsEffect;

const damage = (
  game: Game,
  card: CardState,
  amount: number = 1,
  toOwner: boolean = false
) => {
  const shouldStopDamage = game.card.field
    .map((cardField) => {
      if (cardField) {
        return CardsEffect[CardsId[cardField.cardId]](
          game,
          cardField,
          Trigger.DAMAGE,
          {
            amount: amount,
            toBlue: toOwner == card.isBlue,
          }
        );
      }
    })
    .some((_return) => _return === false);
  if (shouldStopDamage) return;
  if (!card.isBlue == toOwner) {
    if (game.redShield) {
      game.redShield = false;
      game.logs.push(`${card.name} : Le bouclier du joueur Rouge est brisé`);
    } else {
      game.redHP -= amount + game.blueToken.POWER;
      game.logs.push(
        `${card.name} : Le joueur Rouge a perdu ${
          amount + game.blueToken.POWER
        } PV`
      );
    }
  } else {
    if (game.blueShield) {
      game.blueShield = false;
      game.logs.push(`${card.name} : Le bouclier du joueur Bleu est brisé`);
    } else {
      game.blueHP -= amount + game.redToken.POWER;
      game.logs.push(
        `${card.name} : Le joueur Bleu a perdu ${
          amount + game.blueToken.POWER
        } PV`
      );
    }
  }
};

const heal = (
  game: Game,
  card: CardState,
  amount: number = 1,
  toOwner: boolean = true
) => {
  const shouldStopHeal = game.card.field
    .map((cardField) => {
      if (cardField) {
        return CardsEffect[CardsId[cardField.cardId]](
          game,
          cardField,
          Trigger.HEAL,
          {
            amount: amount,
            toBlue: toOwner == card.isBlue,
          }
        );
      }
    })
    .some((_return) => _return === false);
  if (shouldStopHeal) return;
  if (!card.isBlue === toOwner) {
    if (game.redShield) {
      game.redShield = false;
      game.logs.push(`${card.name} : Le bouclier du joueur Rouge est brisé`);
    } else {
      game.redHP += amount;
      game.logs.push(`${card.name} : Le joueur Rouge a récupéré ${amount} PV`);
    }
  } else {
    if (game.blueShield) {
      game.blueShield = false;
      game.logs.push(`${card.name} : Le bouclier du joueur Bleu est brisé`);
    } else {
      game.blueHP += amount;
      game.logs.push(`${card.name} : Le joueur Bleu a récupéré ${amount} PV`);
    }
  }
};

const addShield = (game: Game, card: CardState, toOwner: boolean = true) => {
  const shouldStopShield = game.card.field
    .map((cardField) => {
      if (cardField) {
        return CardsEffect[CardsId[cardField.cardId]](
          game,
          cardField,
          Trigger.SHIELD,
          {
            toBlue: toOwner == card.isBlue,
          }
        );
      }
    })
    .some((_return) => _return === false);
  if (shouldStopShield) return;
  if (!card.isBlue === toOwner) {
    game.redShield = true;
    game.logs.push(`${card.name} : Le joueur Rouge obtient un bouclier`);
  } else {
    game.blueShield = true;
    game.logs.push(`${card.name} : Le joueur Bleu obtient un bouclier`);
  }
};

const popShield = (game: Game, card: CardState, toOwner: boolean = false) => {
  const colorShield = card.isBlue == toOwner ? "blueShield" : "redShield";
  const hadShield = game[colorShield];
  game[colorShield] = false;
  return hadShield;
};

const changeStatus = (
  game: Game,
  card: CardState,
  status: Status = Status.ACTIVATED,
  restore: boolean = false
) => {
  if (restore) {
    card.status &= ~(Status.CAGED | Status.SILENCED);
    game.logs.push(`${card.name} est restoré`);
  } else if (!(card.status == (card.status | status))) {
    card.status |= status;
    game.logs.push(`${card.name} devient ${StatusName[status]}`);
  }
};

const addToDeck = (
  game: Game,
  card: CardState,
  targetCards: Array<CardState>,
  toOwner: boolean = false,
  onTop: boolean = false
) => {
  const deck = !card.isBlue == toOwner ? game.card.redDeck : game.card.blueDeck;
  const playerColor = !card.isBlue == toOwner ? "Rouge" : "Bleu";
  targetCards
    .map((target) => copyCard(target))
    .forEach((target) =>
      deck.splice(
        onTop ? deck.length - 1 : Math.floor(Math.random() * deck.length),
        0,
        {
          ...target,
          isBlue: playerColor == "Bleu",
          isArrowBlue: (playerColor == "Bleu") == target.isArrowBlue,
        }
      )
    );
  game.logs.push(
    `${card.name} : ${targetCards
      .map((card) => card.name)
      .join(", ")} a/ont été ajouté(s) au deck du joueur ${playerColor}`
  );
};

const moveCardRandomly = (
  game: Game,
  card: CardState,
  cardIndex: number = -1
) => {
  const availableSpotsIndex: Array<number> = [];
  game.card.field.forEach((fieldCard, ind) =>
    !fieldCard ? availableSpotsIndex.push(ind) : null
  );
  if (availableSpotsIndex.length == 0) {
    game.logs.push(`${card.name} n'a pas pu bouger`);
    return;
  } else {
    const randomSpotIndex =
      availableSpotsIndex[getRandomInt(availableSpotsIndex.length)];
    game.card.field[randomSpotIndex] = card;
    if (cardIndex >= 0) game.card.field[cardIndex] == null;
    game.logs.push(
      `${card.name} à été déplacé à la case H${(randomSpotIndex % 6) + 1}|V${
        Math.floor(randomSpotIndex / 6) + 1
      }`
    );
  }
};

const peekAtDeck = (
  game: Game,
  card: CardState,
  toOwner: boolean = false,
  amount: number = 3
) => {
  const deckPeeked = card.isBlue ? game.card.redDeck : game.card.blueDeck;
  const cardsPeeked = deckPeeked
    .slice(Math.max(deckPeeked.length - amount, 0), deckPeeked.length)
    .map((deckCard) => deckCard.name);
  game.logs.push(
    `${card.name} : Les ${cardsPeeked.length} prochaines cartes du joueur ${
      card.isBlue == toOwner ? "Bleu" : "Rouge"
    } sont :\n${cardsPeeked.join(", ")}`
  );
};

const peekAtHand = (game: Game, card: CardState) => {
  const handPeeked = card.isBlue ? game.card.redHand : game.card.blueHand;
  const cardsPeeked = handPeeked.map((handCard) => handCard.name);
  game.logs.push(
    `${card.name} : Les cartes en main du joueur ${
      !card.isBlue ? "Bleu" : "Rouge"
    } sont :\n${cardsPeeked.join(", ")}`
  );
};

const morph = (
  game: Game,
  card: CardState,
  targetCard: CardState,
  keepDelay: boolean = true,
  keepStatus: boolean = true
) => {
  const delay = keepDelay ? card.delay : targetCard.delay;
  Object.assign(card, {
    ...targetCard,
    delay,
    isBlue: card.isBlue,
    isArrowBlue: card.isArrowBlue,
    status: keepStatus ? card.status : Status.NONE,
  });
  game.logs.push(`${card.name} est devenu ${targetCard.name}`);
};

const switchColor = (game: Game, card: CardState) => {
  card.isBlue = !card.isBlue;
  card.isArrowBlue = !card.isArrowBlue;
};

const getRandomInt = (max: number): number => {
  return Math.floor(Math.random() * Math.floor(max));
};

const makeModifiedField = <T>(
  field: Array<CardState | null>,
  placeHolders: T
): Array<CardState | null | T> => {
  return Array(9)
    .fill(placeHolders)
    .concat(
      field.slice(0, 6),
      [placeHolders, placeHolders],
      field.slice(6, 12),
      [placeHolders, placeHolders],
      field.slice(12, 18),
      Array(9).fill(placeHolders)
    );
};

const copyCard = (card: CardState) => {
  return JSON.parse(JSON.stringify(card));
};
