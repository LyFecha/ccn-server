/*
const fetch = require("node-fetch");
const fs = require("fs");

console.log(
  /\/cardImg\/([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\/[0-9]\/(COMMON|UNCOMMON|RARE|LEGENDARY|TOKEN)\/[^/]*\/(BLUE|RED)\/(BLUE|RED)/.test(
    "/cardImg/18/7/COMMON/:P: Deal 1 $damage :n: :R: Heal 1/BLUE/RED"
  )
);

const fetches = [];

fetch("http://localhost:3000/cardImg/Adwoa/18/7/COMMON/:P: Deal 1 $damage :n: :R: Heal 1/BLUE/RED")
  .then((res) => {
    const dest = fs.createWriteStream("assets/img.png");
    res.body.pipe(dest);
  })
  .catch((error) => console.log(error));

console.log(fetches);

/*
fetch_node(
  "http://localhost:3000/cardImg/Adwoa/18/7/COMMON/:P: Deal 1 $damage :n: :R: Heal 1/BLUE/RED"
)
  .then((res) => res.json())
  .then((json) => console.log(json))
  .catch((error) => console.log(error));

enum Test {
  A,
  B,
  C,
}

const test = {1:2, 7:5}

const url = "localhost/deck/1:a;5:1;4:2";

const deck: Array<number> = [];
const cardIdAndNb: Array<Array<number>> = [];

try {
  decodeURI(url)
    .split("/")[2]
    .split(";")
    .forEach((idNb) =>
      cardIdAndNb.push(idNb.split(":").map((v) => parseInt(v)))
    );
} catch (error) {
  console.log(error);
}

console.log(cardIdAndNb);

console.log(Test[0]);
*/
