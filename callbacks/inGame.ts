import { find_game } from "../constants";
import { player_in_queue } from "../globals";

const inGame = (req: any, res: any) => {
  console.log(player_in_queue);
  res.send({
    in_game: find_game(req.ip),
    in_queue: player_in_queue.includes(req.ip),
  });
};

export default inGame;
