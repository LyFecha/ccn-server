import Cards from "../model/cards";
import CardsId from "../model/cardsId";
import { decks } from "../globals";

const maxBase = [3, 2, 1, 0];

//Example : /check-deck/1:1;2:1;3:1;4:1;5:1;6:1;7:1;8:1;9:1;10:1;11:1;12:1;13:1;14:1;15:1;16:1;17:1;18:1;19:1;20:1;21:1;22:1;23:1;24:1
const checkDeck = (req: any, res: any) => {
  const deck: Array<number> = [];
  const cardIdAndNb: any = {};

  try {
    decodeURI(req.url)
      .split("/")[2]
      .split(";")
      .forEach((idNb) => {
        const idNbInt = idNb.split(":").map((v) => parseInt(v));
        cardIdAndNb[idNbInt[0]] = cardIdAndNb[idNbInt[0]]
          ? cardIdAndNb[idNbInt[0]] + idNbInt[1]
          : idNbInt[1];
      });
    Object.keys(cardIdAndNb).forEach((id) => {
      const cardName = CardsId[parseInt(id)];
      let max = maxBase[Cards[cardName].base];
      if (cardName && max && parseInt(id) < 500 && cardIdAndNb[id] <= max) {
        deck.splice(
          0,
          0,
          ...(Array(parseInt(cardIdAndNb[id])) as any).fill(id)
        );
      } else {
        throw "Le code est incorrect ou vous avez trop de duplicata";
      }
    });
    console.log(deck.length);
    if (deck.length != 24) {
      throw "Le deck ne contient pas exactement 24 cartes";
    }
  } catch (error) {
    res.send({ response: false, message: error });
    return;
  }
  decks[req.ip] = deck.map((id) => Cards[CardsId[id]]);
  res.send({ response: true, message: "OK" });
};

export default checkDeck;
