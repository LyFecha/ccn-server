import { find_game } from "../constants";
import { current_games } from "../globals";
import Cards from "../model/cards";
import CardsEffect from "../cards/effects";
import { Trigger } from "../cards/effects";
import CardsId from "../model/cardsId";
import activate from "../cards/activate";
import Status from "../model/status";
import CardState from "../model/cardState";

const playerTurn = (req: any, res: any) => {
  console.log("Player-Turn from : " + req.ip);
  const gameId = find_game(req.ip);
  if (gameId > -1) {
    console.log("In game : Checked !");
    const game = current_games[gameId];
    const current_player_blue = game.blueIP == req.ip;
    const [handPos, fieldPos] = decodeURI(req.url)
      .split("/")
      .slice(2, 4)
      .map((v) => parseInt(v));
    // Si ce n'est pas à ce joueur de jouer
    // Sinon si l'endroit où mettre la carte n'est pas libre
    if (!(current_player_blue === game.blueTurn)) {
      console.log("[ERROR] It's not player's turn");
      res.send({ in_game: true, ok: false, game: false });
    } else if (game.card.field[fieldPos]) {
      console.log("[ERROR] Wrong placement for card");
      res.send({ in_game: true, ok: false, game: false });
    } else {
      const current_hand =
        game.card[current_player_blue ? "blueHand" : "redHand"];
      const current_deck =
        game.card[current_player_blue ? "blueDeck" : "redDeck"];
      // On mets sur le terrain
      game.card.field[fieldPos] = current_hand[handPos];
      console.log("Card placed on field");
      // On enlève de la main
      current_hand.splice(handPos, 1);
      console.log("Removed card from hand");
      game.logs.push(
        `Le joueur ${current_player_blue ? "Bleu" : "Rouge"} a joué ${
          game.card.field[fieldPos]!.name
        } à la case H${(fieldPos % 6) + 1}|V${Math.floor(fieldPos / 6) + 1}`
      );
      // On pioche
      for (let i = 0; i < 5 - current_hand.length; i++) {
        current_hand.push(
          current_deck.length > 0 ? current_deck.pop()! : {...Cards["FATIGUE"], isBlue: current_player_blue, isArrowBlue: Cards["FATIGUE"].isArrowBlue == current_player_blue}
        );
        current_deck.length == 0
          ? game.logs.push(
              `Le joueur ${
                current_player_blue ? "Bleu" : "Rouge"
              } n'a plus de carte dans son deck !`
            )
          : null;
        console.log("Card drawn");
      }
      // On fait l'effet des autres cartes quand une carte est joué
      game.card.field.forEach((card, ind) => {
        if (card) {
          CardsEffect[CardsId[card.cardId]](game, card, Trigger.CARD_PLAY, {
            cardPlayed: game.card.field[fieldPos]!,
            cardPlayedId: fieldPos,
            placementId: ind,
          });
        }
      });
      console.log("TRIGGER : Card_Play");
      // On fait l'effet de placement
      CardsEffect[CardsId[game.card.field[fieldPos]!.cardId]](
        game,
        game.card.field[fieldPos],
        Trigger.PLACEMENT,
        { placementId: fieldPos }
      );
      console.log("TRIGGER : Placement");
      // On active les cartes
      activate(game.card.field);
      console.log("Cards activated");
      // On tick les cartes adverse
      const opposite_color_red = current_player_blue ? true : false;
      game.card.field.forEach((card) => {
        if (
          card &&
          card.isBlue !== current_player_blue &&
          (card.status & Status.ACTIVATED) > 0
        ) {
          card.delay -= 1;
        }
      });
      console.log("TRIGGER : Activate");
      // On resolve les cartes adverse
      game.card.field.forEach((card, ind) => {
        if (card && card.isBlue !== current_player_blue && card.delay <= 0) {
          CardsEffect[CardsId[card.cardId]](game, card, Trigger.RESOLVE, {
            placementId: ind,
          });
        }
      });
      console.log("TRIGGER : Resolve");
      // On supprime les cartes avec un delay nul
      game.card.field = game.card.field.map((card) =>
        card && card.delay <= 0 ? null : card
      );
      // On détruit toutes les cartes et inflige 2 de dégats si le tableau est remplie
      if (!game.card.field.some((card) => card === null)) {
        game.card.field = (Array(18) as Array<CardState | null>).fill(null);
        game.blueHP -= 2;
        game.redHP -= 2;
        game.logs.push(
          "Le tableau est rempli ! Tout le monde se prend 2 dégats."
        );
      }
      // On redescend les points de vie à 12 si plus haut
      game.blueHP = Math.min(game.blueHP, 12);
      game.redHP = Math.min(game.redHP, 12);
      // On check s'il y a une victoire
      game.endGame = game.blueHP <= 0 || game.redHP <= 0;
      if (game.blueHP <= 0 && game.redHP <= 0) {
        game.logs.push("Egalité !");
      } else if (game.blueHP <= 0) {
        game.logs.push("Le joueur Rouge a gagné !");
      } else if (game.redHP <= 0) {
        game.logs.push("Le joueur Bleu a gagné !");
      }
      // On envoie les infos comme quoi tout s'est bien passé ;)
      game.blueTurn = !game.blueTurn;
      console.log("End of turn");
      console.log("================================");
      res.send({ in_game: true, ok: true, game });
    }
  } else {
    res.send({ in_game: false, ok: false, game: null });
  }
};

export default playerTurn;
