import { a_game, shuffle, find_game } from "../constants";
import { decks, player_in_queue, current_games } from "../globals";

const play = (req: any, res: any) => {
  if (!decks[req.ip]) {
    res.send({
      in_queue: false,
      in_game: false,
      message: "Aucun deck enregistré",
    });
  } else if (find_game(req.ip) > -1) {
    res.send({
      in_queue: false,
      in_game: true,
      message: "Dans une partie",
      ip: req.ip,
    });
  } else if (player_in_queue.length > 0 && !player_in_queue.includes(req.ip)) {
    const blueIP = player_in_queue.splice(0, 1)[0];
    const redIP = req.ip;
    let blueDeck = shuffle(JSON.parse(JSON.stringify(decks[blueIP])));
    let redDeck = shuffle(
      JSON.parse(
        JSON.stringify(
          decks[redIP].map((card) => {
            card.isArrowBlue = !card.isArrowBlue;
            card.isBlue = false;
            return card;
          })
        )
      )
    );
    const blueHand = blueDeck.slice(blueDeck.length - 5, blueDeck.length);
    const redHand = redDeck.slice(redDeck.length - 5, redDeck.length);
    blueDeck = blueDeck.slice(0, blueDeck.length - 5);
    redDeck = redDeck.slice(0, redDeck.length - 5);

    let a_new_game = JSON.parse(JSON.stringify(a_game));
    a_new_game = {
      ...a_new_game,
      blueIP,
      redIP,
      card: { ...a_new_game.card, blueDeck, redDeck, blueHand, redHand },
      logs: ["C'est le joueur Bleu qui commence à jouer"]
    };
    current_games.push(a_new_game);
    res.send({
      in_queue: false,
      in_game: true,
      message: "La partie commence !",
      ip: req.ip,
    });
  } else {
    if (!player_in_queue.includes(req.ip)) {
      player_in_queue.push(req.ip);
    }
    res.send({ in_queue: true, in_game: false, message: "Dans la queue" });
  }
  console.log("A player wants to play !");
  console.log(Object.keys(decks));
  console.log(player_in_queue);
  console.log(current_games.length);
  console.log("=========================================");
};

export default play;
