import { player_in_queue, current_games } from "../globals";
import { find_game } from "../constants";

const quit = (req: any, res: any) => {
  if (player_in_queue.includes(req.ip)) {
    player_in_queue.push(req.ip);
    res.send({ removed_from_queue: true, removed_from_game: false });
  } else {
    const game_id = find_game(req.ip);
    if (game_id > -1) {
      current_games.splice(game_id, 1);
      res.send({ removed_from_queue: false, removed_from_game: true });
    }
  }
  res.send({ removed_from_queue: false, removed_from_game: false });
};

export default quit;
