import { exec } from "child_process";
import fs = require("fs");
import { root } from "../constants";

//Command line format : python3 python/card.py name arrows delay base description color arrowColor status
//Example : python3 python/card.py "Adwoa" 18 7 COMMON ':P: Deal 1 $damage :n: :R: Heal 1' BLUE RED 0
//Path format : /cardImg/<name>/0-255/0-9/COMMON ou UNCOMMON ou RARE ou TOKEN/<description>/BLUE ou RED/BLUE ou RED/0-15
//Example : /cardImg/Adwoa/18/7/COMMON/:P: Deal 1 $damage :n: :R: Heal 1/BLUE/RED/0
const cardImg = (req: any, res: any) => {
  console.log("Image url : " + req.url);
  const cardCharac = decodeURI(req.url).split("/").slice(2, 9);
  exec(`python3 "./python/card.py" "${cardCharac.join('" "')}"`, (error) => {
    if (error) {
      console.log(error);
    } else {
      const imgFileName = `./python/img_${cardCharac
        .slice(0, 4)
        .concat(cardCharac.slice(5))
        .join("_")
        .replace(/\s/g, "_")}.png`;
      res.sendFile(imgFileName, { root: root }, () => deleteFile(imgFileName));
    }
  });
};

const deleteFile = (imgFileName: string) => {
  fs.unlink(imgFileName, (err) => {
    if (err) {
      console.log("Error : " + err);
    } else {
      console.log("Image deleted");
    }
  });
};

export default cardImg;
