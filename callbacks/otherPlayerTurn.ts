import { find_game } from "../constants";
import { current_games } from "../globals";

const otherPlayerTurn = (req: any, res: any) => {
  console.log("Other-Player-Turn from : " + req.ip);
  const gameIndex = find_game(req.ip);
  if (gameIndex > -1) {
    const game = current_games[gameIndex];
    const current_player_blue = req.ip === game.blueIP;
    res.send({
      in_game: true,
      other_player_turn: !game.blueTurn === current_player_blue,
      game: game,
    });
  } else {
    res.send({ in_game: false, other_player_turn: null, game: null });
  }
};

export default otherPlayerTurn;
