import { root } from "../constants";

const apatheticFrog = (req:any, res:any) =>
  res.sendFile("assets/Apathetic Frog.png", {
    root: root,
  });

export default apatheticFrog;
