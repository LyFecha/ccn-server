const express = require("express");
var cors = require("cors");

import "./globals";
import { port } from "./constants";

import play from "./callbacks/play";
import quit from "./callbacks/quit";
import cardImg from "./callbacks/cardImg";
//import inGame from "./callbacks/inGame";
import apatheticFrog from "./callbacks/apatheticFrog";
import checkDeck from "./callbacks/checkDeck";
import playerTurn from "./callbacks/playerTurn";
import otherPlayerTurn from "./callbacks/otherPlayerTurn";

const app = express();

app.use(function (req: any, res: any, next: Function) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get(/\/check-deck\/[\d:;]*/, checkDeck);

app.get("/play", play);

//app.get("/in-game", inGame);

app.get("/quit", quit);

app.get(/\/player-turn\/[0-4]\/([0-9]|1[0-7])/, playerTurn);

app.get(
  /\/cardImg\/[^/\s]*\/([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\/[0-9]\/(COMMON|UNCOMMON|RARE|TOKEN)\/[^/]*\/(BLUE|RED)\/(BLUE|RED)\/(0?[0-9]|1[0-5])/,
  cardImg
);

app.get("/other-player-turn", otherPlayerTurn);

app.get("/apathetic-frog", apatheticFrog);

app.listen(port, "0.0.0.0", () =>
  console.log(`App listening at port: ${port}`)
);
