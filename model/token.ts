enum Token {
  MANA = "MANA",
  POWER = "POWER",
  FATIGUE = "FATIGUE",
}

export default Token;
