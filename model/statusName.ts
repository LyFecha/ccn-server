import Status from "./status";

const StatusName : {[index: string] : String} = {
  NONE: "sans statut",
  CAGED: "enfermée",
  SILENCED: "muette",
  NON_CONDUCTIVE: "non conductible",
  ACTIVATED: "activée",
}

export default StatusName;
