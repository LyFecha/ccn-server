enum Status {
  NONE = 0,
  CAGED = 1,
  SILENCED = 1 << 1,
  NON_CONDUCTIVE = 1 << 2,
  ACTIVATED = 1 << 3,
}

export default Status;
