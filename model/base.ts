enum Base {
  COMMON,
  UNCOMMON,
  RARE,
  TOKEN,
  UNKNOWN,
}

export default Base;
