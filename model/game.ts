import CardState from "./cardState";

type Game = {
  blueIP: String;
  redIP: String;
  blueHP: number;
  redHP: number;
  blueShield: boolean;
  redShield: boolean;
  blueToken: { [index: string]: number };
  redToken: { [index: string]: number };
  blueTurn: boolean;
  endGame: boolean;
  card: {
    blueDeck: Array<CardState>;
    redDeck: Array<CardState>;
    blueHand: Array<CardState>;
    redHand: Array<CardState>;
    field: Array<CardState | null>;
  };
  logs: Array<String>;
};

export default Game;
