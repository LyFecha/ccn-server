import Game from "./model/game";
import CardState from "./model/cardState";

export const player_in_queue: Array<string> = [];
export const current_games: Array<Game> = [];
export const decks: { [index: string]: CardState[] } = {};
