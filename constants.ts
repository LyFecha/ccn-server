import game from "./model/game";
import { current_games } from "./globals";
import CardState from "./model/cardState";

export const port = 80;
export const root = "./";

export const a_game: game = {
  blueIP: "0.0.0.0",
  redIP: "0.0.0.0",
  blueHP: 10,
  redHP: 10,
  blueShield: false,
  redShield: false,
  blueToken: { MANA: 0, POWER: 0, FATIGUE: 0 },
  redToken: { MANA: 0, POWER: 0, FATIGUE: 0 },
  blueTurn: true,
  endGame: false,
  card: {
    blueDeck: [],
    redDeck: [],
    blueHand: [],
    redHand: [],
    field: (Array(18) as Array<CardState | null>).fill(null),
  },
  logs: [],
};

export const find_game = (ip: string) => {
  const is_game: any = current_games.map(
    (game) => game.blueIP == ip || game.redIP == ip
  );
  return (is_game as any).findIndex((element: boolean) => element);
};

export const shuffle = (deck: Array<CardState>) => {
  const array = deck.slice();
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};
