from PIL import Image, ImageDraw, ImageFont
from math import sqrt
import sys
import os
from collections import defaultdict

root = os.path.abspath(os.path.dirname(sys.argv[0]))
# fontRoot = ""
fontRoot = "/home/fanny_maxence_techno_mobile/ccn-server/python/"


def add(it1, it2):
    it = ()
    for i in range(len(it1)):
        it += tuple([it1[i]+it2[i]])
    return it


def mult(it1, m):
    it = ()
    for v in it1:
        it += tuple([v*m])
    return it


def bold(draw, pos, text, color=(255, 255, 255, 255)):
    for i in range(2):
        draw.text((pos[0]+i, pos[1]), text, fill=color, font=TEXT_FONT)


COLOR = {"BLUE": "_0", "RED": "_1"}
FRAME = {"TOKEN": "_dark", "COMMON": "",
         "UNCOMMON": "_uncommon", "RARE": "_rare"}
FORMAT = defaultdict(lambda: False, {
    ":P:": "$Placement:",
    ":D:": "$Draw:",
    ":R:": "$Resolve:",
    ":NC:": "$Non-conductive",
    ":U:": "$Untargetable",
    ":n:": "\n",
})
STATUS = {
    "NONE": 0,
    "CAGED": 1,
    "SILENCED": 1 << 1,
    "NON_CONDUCTIVE": 1 << 2,
    "ACTIVATED": 1 << 3,
}
CARD_SIZE = {"H": 670, "V": 897}
IMG_OFFSET = {"H": 76, "V": 56}
IMG_SIZE = {"H": 512, "V": 342}
ARROW_SIZE = {"H": 86, "V": 71}
N_ARROW = 8
ARROW_RELATIVE_PLACE = [
    (0, 0), (1/2, 0), (1, 0), (1, 1/2),
    (1, 1), (1/2, 1), (0, 1), (0, 1/2)
]
ARROW_PLACE = [(int(x[0]*(CARD_SIZE["H"]-ARROW_SIZE["H"])), int(x[1]
                                                                * (CARD_SIZE["V"]-ARROW_SIZE["V"]))) for x in ARROW_RELATIVE_PLACE]
ARROW_CROP = [(1, 1, 0, 0), (0, 1, -1, 0), (0, 0, -1, -1), (1, 0, 0, -1)]
ARROW_H_OFFSET = [-14, 0, 14, 14, 14, 0, -14, -14]
TITLE_FONT = ImageFont.truetype(
    fontRoot+"segoeprb.ttf", 32)
TITLE_BOX = [86, 424-12, 444, 460]
TEXT_FONT = ImageFont.truetype(
    fontRoot+"negotiate free.ttf", 42)
TEXT_BOX = [104, 520, 564, 790]
CHARGE_FONT = ImageFont.truetype(
    fontRoot+"MAIAN.TTF", 92)
CHARGE_CENTER = [542, 416-10]

PAGE_SIZE = [10, 7]


class Card:

    def __init__(self, name, arrows, delay, frame, effect, color, arrowsColor, status="0"):
        self.name = name
        self.imgName = "".join(map(lambda x: x[0].upper()+x[1:], name.split()))
        self.arrows = int(arrows)
        self.delay = int(delay)
        self.frame = FRAME[frame]
        self.effect = effect
        self.color = COLOR[color]
        self.arrowsColor = COLOR[arrowsColor]
        self.status = int(status)

    def draw(self):
        card = Image.new(
            "RGBA", (CARD_SIZE["H"], CARD_SIZE["V"]), (0, 0, 0, 0))
        img = Image.open(root+"/Sprite/CardImg/" +
                         self.imgName+".png").convert("RGBA")
        frame = Image.open(root+"/Sprite/CardUI/CardBase" +
                           self.color+self.frame+".png").convert("RGBA")
        frame = frame.resize((CARD_SIZE["H"], CARD_SIZE["V"]))

        # Image
        card.paste(img, (IMG_OFFSET["H"], IMG_OFFSET["V"], ), img)

        # Cage
        if (self.status & STATUS["CAGED"]) == STATUS["CAGED"]:
            cage = Image.open(
                root+"/Sprite/CardUI/CardCage.png").convert("RGBA").resize((IMG_SIZE["H"], IMG_SIZE["V"]))
            card.paste(cage, (IMG_OFFSET["H"], IMG_OFFSET["V"]), cage)

        # Frame
        card.paste(frame, (0, 0), frame)

        # Arrow
        if not((self.status & STATUS["CAGED"]) == STATUS["CAGED"] or (self.status & (STATUS["NON_CONDUCTIVE"] | STATUS["ACTIVATED"])) == (STATUS["NON_CONDUCTIVE"] | STATUS["ACTIVATED"])):
            for i in range(N_ARROW):
                if (self.arrows >> i) & 0b1:
                    arrow = Image.open(root+"/Sprite/CardUI/CardArrow"+self.arrowsColor+".png").convert("RGBA").resize(
                        (ARROW_SIZE["H"], ARROW_SIZE["V"])).crop(add((-10, -10, 10, 10), (0, 0, ARROW_SIZE["H"], ARROW_SIZE["V"]))).rotate(-(i-1)*45)
                    cropPlace = (1, 1, -1, -1)
                    if i % 2 == 0:
                        cropPlace = ARROW_CROP[i//2]
                    arrow = arrow.crop(
                        add(mult(cropPlace, 10), (0, 0, arrow.size[0], arrow.size[1])))
                    notCropPlace = [1-v for v in cropPlace[:2]
                                    ] + [-1-v for v in cropPlace[2:]]
                    card.paste(arrow, add(
                        add(mult(notCropPlace[0: 2], -10), ARROW_PLACE[i]), (ARROW_H_OFFSET[i], 0)), arrow)

        # Silence
        if (STATUS["SILENCED"] & self.status) == STATUS["SILENCED"]:
            silence = Image.open(root+"/Sprite/CardUI/CardSilence.png").convert(
                "RGBA").resize((TEXT_BOX[2] - TEXT_BOX[0], TEXT_BOX[3] - TEXT_BOX[1]))
            card.paste(silence, (TEXT_BOX[0], TEXT_BOX[1]), silence)

        cardDraw = ImageDraw.Draw(card)

        # Desc
        if not((STATUS["SILENCED"] & self.status) == STATUS["SILENCED"]):
            SPACE_SIZE = cardDraw.textsize(" ", font=TEXT_FONT)
            textSplit = [FORMAT[word] if FORMAT[word]
                         else word for word in self.effect.split()]
            lineNb = 0
            colPos = 0
            for word in textSplit:
                textSize = [cardDraw.textsize(word, font=TEXT_FONT)[
                    0]]+[SPACE_SIZE[1]]
                if word == "\n" or textSize[0] + colPos > TEXT_BOX[2]-TEXT_BOX[0]:
                    lineNb += 1.5 if word == "\n" else 1.2
                    colPos = 0
                if textSize[0] + colPos < TEXT_BOX[2] and word != "\n":
                    word += " "
                    if word[0] == "$":
                        bold(
                            cardDraw, (TEXT_BOX[0]+colPos, TEXT_BOX[1]+lineNb*textSize[1]), word[1:])
                        colPos += textSize[0] + SPACE_SIZE[0] + 1 - \
                            cardDraw.textsize("$", font=TEXT_FONT)[0]
                    elif word[0] == "µ":
                        bold(cardDraw, (TEXT_BOX[0]+colPos, TEXT_BOX[1] +
                                        lineNb*textSize[1]), word[1:], (184, 251, 255, 255))
                        colPos += textSize[0] + SPACE_SIZE[0] + 1 - \
                            cardDraw.textsize("µ", font=TEXT_FONT)[0]
                    else:
                        cardDraw.text((TEXT_BOX[0]+colPos, TEXT_BOX[1]+lineNb*textSize[1]),
                                      word, fill=(184, 251, 255, 255), font=TEXT_FONT)
                        colPos += textSize[0] + SPACE_SIZE[0]

        # Title
        cardDraw.text((TITLE_BOX[0], TITLE_BOX[1]), self.name, fill=(
            0, 0, 0, 255), font=TITLE_FONT)

        # Charge
        chargeSize = cardDraw.textsize(str(self.delay), font=CHARGE_FONT)
        cardDraw.text((CHARGE_CENTER[0]-chargeSize[0]/2, CHARGE_CENTER[1]-chargeSize[1]/2), str(
            self.delay), fill=(28, 177, 255, 255) if self.color == COLOR["BLUE"] else (255, 0, 28, 255), font=CHARGE_FONT)
        return card


print(sys.argv[1:])
card = Card(*sys.argv[1:])
c = card.draw()
c.save(
    root+"/img_{}.png".format("_".join(sys.argv[1:5]+sys.argv[6:]).replace(" ", "_")))
